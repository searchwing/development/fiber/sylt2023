# Sylt 2023

Preparations for the Summercamp 2023 Sylt

## Packliste

[Sylt Packliste](./packlist-sylt.md)

## Parts ordered / parts missing

| Qty. | Part | Dealer | Status | Date | Remark |
| ------ | ------ | -- | -- | -- | -- |
|  2 | Aeronaut Anlenkung | Ebay | received | 14.6.23 | - |
|  6 | Krick Anlenkung | Modellbau Berthold | received | 14.6.23 | - |
|  3 | SLS XT26004120 Akku | Stefans Lipo Shop | received | 14.6.23 | - |
|  4 | Matek H743 Wing | Premium Modellbau | received | 19.6.23 | - |
|  3 | Matek CAN GPS | Premium Modellbau | received | 19.6.23 | - |
|  4 | Holybro Tekko F4 | Premium Modellbau | received | 19.6.23 | - |
|  2 | RFD868x Bundle (4 Modems) | Mybotshop | received | 19.6.23 | - |
|  2 | Taoglas 868MHz Antenna | Mouser | received | 19.6.23 | - |
|  2 | Radiomaster TX16S inkl. Akku | Flyingmachines | received | 19.6.23 | - |
|  1 | 4H9 Grenzlehrdorn | Ebay | received | 16.6.23 | - |
|  1 | 1m Alurohr 60x5mm | Metall Dehner | received | 13.6.23 | - |
|  25 | 4x8x2 Wellendichtring | Diehr&Rabenstein | received | 14.6.23 | - |
|  4 | Drehteil: Buchse Leitwerk | Höhne | ordered | 15.6.23 | 2-3 Wochen Lieferzeit |
|  8 | Drehteil: Rumpfsektionsverbinder | Höhne | ordered | 15.6.23 | 2-3 Wochen Lieferzeit |
|  3 | 1m 50x48mm GFK Rohr | CG-Tec | ordered | 15.6.23 | 4-6 Wochen Lieferzeit|
|  4 | Extron 2814/20 Motor| MIH-Toys, Jasper, Eberhardt, Berlinski| ordered| 16.06.23|Marco|
|  12 | Präzisions O-Ring 50,52 x 1,78 mm EPDM| HUG-Technik | ordered| 16.06.23| Marco|
|  1 | 3M DP-490 Kleber| Tewishop | received| 26.06.23| Fritz |
|  2 | CFK Rohr 50x48mm | R&G | ordered | 26.06.23| Fritz |
|  8 | Stopfen SLA Print | JLCPCB | ordered | 23.06.23| Fritz |
|  10 | 38x5mm O-Ring | HUG-Technik | ordered | 26.06.23| Fritz |







