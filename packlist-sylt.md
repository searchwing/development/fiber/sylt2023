# Packliste Sylt

## Faserflieger

* 1 x Rumpf Leitwerkssektion
* 1 x Rumpf Elektroniksektion
* 1 x Rumpf Akkusektion
* 1 x VTail links
* 1 x VTail rechts
* 1 x Hauptflügel
* 60 x Kabelbinder 300mmx4.6mm
* 3 x GFK Flügelhalter vorn/hinten
* 2 x Flügelplatte oben vorn
* 2 x Flügelplatte oben hinten
* 100 x M3x45 Nylonschrauben Senkkopf
* 20 x Unterlegscheiben M3 Senkkopf nach flach
* 100 x Unterlegscheiben Nylon M3
* 100 x M3x40 Nylonschrauben Flachkopf

## Laden

XT60/90 Buchse ist das Steckerteil am Akku.

* 2 x Voltcraft V-Charge 50 Ladegerät
* 2 x Kaltegerätekabel für Ladegerät
* 1 x Banane nach XT90 Stecker zum Anschluss von Akku an Ladegerät
* 3 x Banane nach XT60 Stecker zum Anschluss von Akku an Ladegerät
* 1 x XT90 Buchse nach Weipu SP2112/P2 zum Anschluss des Akkus an das Ladegerät
* 2 x XT60 Buchse nach XT90 Stecker
* 1 x XT60 Stecker auf XT90 Buchse
* 1 x Banane nach XT60 Buchse um einen Akku durch ein Netzteil zu ersetzen
* 1 x XT60 Buchse nach Weipu SP2112/P2 zum Anschluss des Akuus an das Ladegerät

## Akkus

* 3 x Akku SLS XTRON 4S/2600mAh mit EPP Halterung XT60 (für Faserflieger)
* 1 x Hacker ECO-X 4S/4500mAh XT60
* 2 x SLS XTRON 4S/10000mAh wasserdicht Weipu (Augsburg)
* 1 x SLS XTRON 4S/10000mAh XT90
* 2 x Turnigy 4S/4000mAh XT60
* 1 x SLS 4S/6750mAh XT90
* 2 x Batsafe


## Groundstation

* 2 x Taoglas 868 MHz Antenne
* 1 x Groundstation Classic (Eth2Uart, Aux) mit RFD868x Modem

* 1 x Groundstation Philipp (Berlin)
* 2 x Spelsberg MSS-30 Masthalterung (Berlin/Aux)






